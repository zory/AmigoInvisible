# Amigo invisible

To run the program, first create "settings.py" in this directory with the following structure:

```python
# -*- coding: utf-8 -*-

# Information of the email account that sends all the emails
SENDER = "sender of the emails"
SENDER_PASSWORD = "password of the sender"
SMTP_SERVER = "smtp server of the sender (smtp.gmail.com for gmail)"
# List of friends
EMAIL_LIST = [('Name1', 'email of name1'), ('Name2', 'email of name2'), ('Name3', 'email of name3')...]
```

Finally run "main.py" and enjoy
